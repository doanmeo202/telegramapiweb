package com.cathay.ai.aa.a0.modun;

import com.cathay.ai.aa.a0.trx.AAA0_0100;
import com.cathay.ai.aa.a0.vo.AAA0_0100_vo;
import boofcv.abst.fiducial.QrCodeDetector;
import boofcv.alg.fiducial.qrcode.QrCode;
import boofcv.factory.fiducial.ConfigQrCode;
import boofcv.factory.fiducial.FactoryFiducial;
import boofcv.io.UtilIO;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayU8;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;

public class AAA0_0100_mod {
	private String osName = ObjectUtils.toString(System.getProperties().get("os.name"));
	private static final Logger log = Logger.getLogger(AAA0_0100_mod.class);

//	static {
//		try {
//			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
//		} catch (Exception e) {
//			log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//		}
//	}

//	public Map aiQrCodeDetectFromImage(String path) {
//		Map Oup = new HashMap();
//		try {
//			Mat img = Imgcodecs.imread(path);
//			QRCodeDetector decoder = new QRCodeDetector();
//			Mat points = new Mat();
//			String data = decoder.detectAndDecode(img, points);
//			if (!points.empty()) {
//				log.debug("OpenCV decoded data: [" + data + "]");
//			}
//			Oup.put("returnCode", "0");
//			Oup.put("returnMess", "Hoan Thanh Nhan Dien Qr Code");
//			Oup.put("data", data);
//		} catch (Exception e) {
//			Oup.put("returnCode", "-1");
//			Oup.put("returnMess", e.getCause());
//			Oup.put("data", null);
//		}
//		return Oup;
//	}


//	public static void main(String[] args) {
//	String path = "D:/ADS.png";
//		String qRout = "D:/QRout.PNG";
//		Mat img = Imgcodecs.imread(path,0);
//		QRCodeDetector decoder = new QRCodeDetector();
//		Mat points = new Mat();
//		String data = decoder.detectAndDecode(img, points);
//	
//		Mat mat = new Mat();
//		List<MatOfPoint> contours = new ArrayList();
//		Mat dest = Mat.zeros(mat.size(), CvType.CV_8UC3);
//		Scalar white = new Scalar(255, 255, 255);
//		// Find contours
//		Imgproc.findContours(img, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
//		Imgproc.drawContours(dest, contours, -1, white);
//		Imgcodecs.imwrite(qRout, img);
//	
//	}

	public static AAA0_0100_vo detectQRCode(BufferedImage input) {
		
		AAA0_0100_vo vo = new AAA0_0100_vo();
		
		String returnCode="0";// 0 success ; -1 err
		ConfigQrCode config = new ConfigQrCode();
		
		log.debug("Config  : [" + config.considerTransposed + "];  //false by default");
		log.debug("Note    : config.considerTransposed [fasle];//Faster decoding");
		log.debug("Note    : config.considerTransposed [true]; //return utf8 Decoding slower");
		if(input==null) {
			vo.setReturnCode("-1");
			vo.setReturnMess("Vui lòng kiểm tra lại file hình ảnh");
			vo.setData(null);
			return vo;
		}
		GrayU8 gray = ConvertBufferedImage.convertFrom(input, (GrayU8) null);
		QrCodeDetector<GrayU8> detector = FactoryFiducial.qrcode(config, GrayU8.class);
		detector.process(gray);
		List<QrCode> detections = detector.getDetections();
		List<QrCode> failures = detector.getFailures();
		String Mess="";
		
		String data="";
		if(detections.size()>0) {
			log.debug("Detect  : [" + detections.size() + "]");
			for (QrCode qr : detections) {
				log.debug("Message : '" + qr.message + "'");
				data+="#"+qr.message;
			}
			Mess+="Số QRcode hoàn thành giải mã :"+detections.size();
			
			vo.setData(data);
			vo.setReturnCode(returnCode);
			vo.setReturnMess(Mess);
		}else {
			Mess+="Không Phát hiện QRcode";
			vo.setData(null);
			vo.setReturnCode("-3");
			vo.setReturnMess(Mess);
			log.debug("QrUnKnow: []");
			if(failures.size()>0) {
				log.debug("QrUnKnow: [" + failures.size() + "]");
				Mess+=" => Giải mã thất bại "+failures.size()+" hình ảnh";
				vo.setData(null);
				vo.setReturnCode("-2");
				vo.setReturnMess(Mess);
			}
		}
		
		
		
		return vo;

	}
	 public void writeImage(BufferedImage image) throws IOException {
		   if (osName.startsWith("Wind")) {
				File outputfile = new File("D:/png/Scan.png");
				ImageIO.write(image, "png", outputfile);
			} else {
				File outputfile = new File("/opt/tomcat/webapps/Img/Scan.png");
				ImageIO.write(image, "png", outputfile);
			}
	}
	public static void main(String[] args) {
		BufferedImage input = UtilImageIO.loadImage(UtilIO.pathExample("D:/VVVV.PNG"));
		log.debug(detectQRCode(input).getReturnMess());

//		log.debug("=====================================================================================");
//		GrayU8 gray = ConvertBufferedImage.convertFrom(input, (GrayU8) null);
//		ConfigQrCode config = new ConfigQrCode();
//		log.debug("Config : [" + config.considerTransposed + "]; //false by default");
//		QrCodeDetector<GrayU8> detector = FactoryFiducial.qrcode(config, GrayU8.class);
//		detector.process(gray);
//		List<QrCode> detections = detector.getDetections();
//		log.debug("Detect : [" + detections.size() + "]");
//
//		Graphics2D g2 = input.createGraphics();
//		int strokeWidth = Math.max(4, input.getWidth() / 200); // in large images the line can be too thin //
//		g2.setColor(Color.GREEN); // g2.setStroke(new BasicStroke(strokeWidth)); //
//		for (QrCode qr : detections) {
//			log.debug("message: '" + qr.message + "'");
//			VisualizeShapes.drawPolygon(qr.bounds, true, 1, g2);
//		}
//
//		List<QrCode> failures = detector.getFailures();
//		log.debug("QrUnKnow: [" + failures.size() + "]");
//		g2.setColor(Color.RED);
//		for (QrCode qr : failures) { // // If the 'cause' is ERROR_CORRECTION or higher, then there's a decent chance
//										// it's a real marker //
//			if (qr.failureCause.ordinal() < QrCode.Failure.ERROR_CORRECTION.ordinal())
//				continue;
//			VisualizeShapes.drawPolygon(qr.bounds, true, 1, g2);
//		}
//		log.debug("=====================================================================================");
//		ShowImages.showWindow(input, "Example QR Codes", true);
	}
}
