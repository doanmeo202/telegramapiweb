package com.cathay.ai.aa.a0.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.ObjectUtils;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;


import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class JdbcHelper {
	static Logger logger = Logger.getLogger(JdbcHelper.class.getName());

	public Connection getConnection() throws ClassNotFoundException {

		Sql2o JdbcHeper = new Sql2o("jdbc:mysql://115.76.106.243:3306/DB_STAG?useUnicode=yes&characterEncoding=UTF-8",
				"cxluser", "Thanh0974135042!");
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = JdbcHeper.beginTransaction();
		return con;
	}

	public List<Map<String, Object>> getDataFromKey(String keysql, Map map) {
		List<Map<String, Object>> map1 = new ArrayList();
		List<Map<String, Object>> mapoutput = new ArrayList();
		try {
			Connection con = getConnection();

			Connection con1 = getConnection();
			System.out.println("SET PARAM [CODE_NAME=" + keysql + "]");
			map1 = con.createQuery("SELECT * from DTCM0004_CODEMAPSQL where CODE_NAME like :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();
			if (map1.size() < 1) {
				con.close();
				con1.close();
				throw new Sql2oException("NOT FOUND CODE_NAME:" + keysql);
			}
			if (map1 != null || map1.size() > 0) {
				String SQLSTRING = ObjectUtils.toString(map1.get(0).get("sqlstring"));
				System.out.println("SQL STRING = [" + SQLSTRING + "]");
				System.out.println("CODE_NAME: " + keysql);
				if (!SQLSTRING.equals("")) {
					if (map != null || map.size() > 0) {
						Query query = con1.createQuery(SQLSTRING);

						ArrayList myKeyList = new ArrayList(map.keySet());

						for (int i = 0; i < myKeyList.size(); i++) {

							String key = (String) myKeyList.get(i);
							String value = (String) map.get(myKeyList.get(i));
							System.out.println("SET PARAM [" + key + "=" + value + "]");
							query.addParameter(key, value);
						}

						mapoutput = query.executeAndFetchTable().asList();
						con.close();
						con1.close();
					} else {
						mapoutput = con1.createQuery(SQLSTRING).executeAndFetchTable().asList();
						con.close();
						con1.close();
					}
				}
			}

			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return mapoutput;
	}
	public void setDataFromKey(String keysql, Map map) {
		List<Map<String, Object>> map1 = new ArrayList();
		List<Map<String, Object>> mapoutput = new ArrayList();
		
		try {
			Connection con = getConnection();
			Connection con1 = getConnection();
			System.out.println("SET PARAM [CODE_NAME=" + keysql + "]");
			map1 = con.createQuery("SELECT * from DTCM0004_CODEMAPSQL where CODE_NAME like :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();
			if (map1.size() < 1) {
				 con.close();
				 con1.close();
				throw new Sql2oException("NOT FOUND CODE_NAME:" + keysql);
			}
			if (map1 != null || map1.size() > 0) {
				String SQLSTRING = ObjectUtils.toString(map1.get(0).get("sqlstring"));
				System.out.println("SQL STRING = [" + SQLSTRING + "]");
				System.out.println("CODE_NAME: " + keysql);
				if (!SQLSTRING.equals("")) {
					if (map != null || map.size() > 0) {
						Query query = con1.createQuery(SQLSTRING);

						ArrayList myKeyList = new ArrayList(map.keySet());

						for (int i = 0; i < myKeyList.size(); i++) {

							String key = (String) myKeyList.get(i);
							String value = (String) map.get(myKeyList.get(i));
							
							System.out.println("SET PARAM [" + key + "=" + value + "]");
							query.addParameter(key, value);
						}
						   query.executeUpdate().commit();
						   con.close();
						   con1.close();
						
					} else {
						 con1.createQuery(SQLSTRING).executeUpdate().commit();
						 con.close();
						 con1.close();
					}
				}
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	//	return mapoutput;
	}

	public List<Map<String, Object>> QueryCODE_MAPLikeAll(String keysql) {
		List<Map<String, Object>> ListArr = new ArrayList();
		try {
			Connection con = getConnection();
			ListArr = con.createQuery("SELECT * FROM DTCM0004_CODEMAPSQL A where A.CODE_NAME LIKE '%" + keysql + "%'")
					.executeAndFetchTable().asList();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return ListArr;

	}

	public List<Map<String, Object>> QueryCODE_MAP(String keysql) {
		List<Map<String, Object>> ListArr = new ArrayList();
		try {
			Connection con = getConnection();
			ListArr = con.createQuery("SELECT * FROM DTCM0004_CODEMAPSQL A where A.CODE_NAME LIKE :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return ListArr;

	}

	public int InsertCODE_MAP(String keysql, String Content) {
		
		List<Map<String, Object>> ListArr = new ArrayList();
		int check = 0;
		try {
			Connection con = getConnection();
			ListArr = con.createQuery("SELECT * FROM DTCM0004_CODEMAPSQL A where A.CODE_NAME LIKE :CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeAndFetchTable().asList();
			if (ListArr.size() >= 1) {
				throw new Sql2oException("data not exit with key:" + keysql);
			} else {
				Connection con1 = getConnection();
				con1.createQuery(
						"INSERT INTO DTCM0004_CODEMAPSQL (CODE_NAME, SQLSTRING, FLAG) VALUES(:CODE_NAME,:SQLSTRING,0)")
						.addParameter("CODE_NAME", keysql).addParameter("SQLSTRING", Content).executeUpdate();
				check =con1.getResult();
				con1.commit();
				con1.close();
//				AAB0_0100_mod mod= new AAB0_0100_mod();
//				Map mapBackup= new HashMap();
//				mapBackup.put("CODE_NAME", keysql);
//				mapBackup.put("SQLSTRING", Content);
//				mod.backUpCodeMap(mapBackup);
			}
			con.close();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			check = 0;
		}
		System.out.println(">>>>>>>>>>>>>>>>>>>CHECK: " + check);
		return check;

	}

	public int updateCODE_MAP(String keysql, String Content) {
		List<Map<String, Object>> ListArr = new ArrayList();
		List<Map<String, Object>> ListArr2 = new ArrayList();
		int check = 0;
		try {
			Connection con = getConnection();
			con.createQuery("UPDATE DTCM0004_CODEMAPSQL SET  SQLSTRING=:SQLSTRING WHERE CODE_NAME=:CODE_NAME")
					.addParameter("CODE_NAME", keysql).addParameter("SQLSTRING", Content).executeUpdate();
			check =con.getResult();
			con.commit();
			con.close();
//			AAB0_0100_mod mod= new AAB0_0100_mod();
//			Map mapBackup= new HashMap();
//			mapBackup.put("CODE_NAME", keysql);
//			mapBackup.put("SQLSTRING", Content);
//			mod.backUpCodeMap(mapBackup);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			check = 0;
		}
		System.out.println(">>>>>>>>>>>>>>>>>>>CHECK: " + check);
		return check;

	}

	public int deleteCODE_MAP(String keysql) {
		
		int check = 0;
		try {
			Connection con = getConnection();
			con.createQuery("DELETE FROM  DTCM0004_CODEMAPSQL WHERE CODE_NAME=:CODE_NAME")
					.addParameter("CODE_NAME", keysql).executeUpdate();
			logger.fatal("[DELETE FROM  DTCM0004_CODEMAPSQL WHERE CODE_NAME="+keysql+"]");
			logger.fatal("[CODE_NAME="+keysql+"]");
			check =con.getResult();
			logger.fatal("[UPDATE RESURL="+check+"]");
			con.commit();
			con.close();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			check = -1;
		}
		System.out.println(">>>>>>>>>>>>>>>>>>>CHECK: " + check);
		return check;

	}

	public JdbcHelper() {
	}
	public String getPackage(String name, String simpleName) {
		int indexOf = name.indexOf("." + simpleName);
		return name.substring(0, indexOf);
	}
	public static void main(String[] args) throws UnknownHostException {
		BasicConfigurator.configure();
//		System.out.println("Hello world");
//		System.out.println("we are in logger info mode");
//		logger.fatal("hi");
		
		
		
	}

}
