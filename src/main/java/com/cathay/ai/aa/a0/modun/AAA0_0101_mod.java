package com.cathay.ai.aa.a0.modun;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.FilenameUtils;

import com.cathay.ai.aa.a0.vo.AAA0_0100_vo;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.ImageEncoder;
import com.sun.media.jai.codec.JPEGEncodeParam;
import com.sun.media.jai.codec.TIFFDecodeParam;

public class AAA0_0101_mod {
	
	static String logImage = "                 \\||/\r\n" + "                 |  @___oo\r\n"
			+ "       /\\  /\\   / (__,,,,|\r\n" + "      ) /^\\) ^\\/ _)\r\n" + "      )   /^\\/   _)\r\n"
			+ "      )   _ /  / _)\r\n" + "  /\\  )/\\/ ||  | )_)\r\n" + " <  >      |(,,) )__)\r\n"
			+ "  ||      /    \\)___)\\\r\n" + "  | \\____(      )___) )___\r\n" + "   \\______(_______;;; __;;;\r\n"
	        +" ================================================DoanNgocThanh=========================================";

	static {
		System.out.println(logImage);
	}
	public static String readQRCode(String filePath, String charset, Map hintMap)
			throws FileNotFoundException, IOException, NotFoundException {
		BinaryBitmap binaryBitmap = new BinaryBitmap(
				new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(new FileInputStream(filePath)))));
		Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap, hintMap);
		return qrCodeResult.getText();
	}

	public static void main(String[] args) throws IOException, NotFoundException, ChecksumException, FormatException {
		File fi = new File("D:/01MX00706942.tif");
		InputStream is = new FileInputStream(fi);
		List li=new ArrayList();
		li.add(18);
		
		System.out.println(getBarcode(is, 20, 2, null));
	}
	public static List<Map> getBarcode(InputStream ips,int rows,int cols,List pageIndexGetSplit) {
		List<Map> li = new ArrayList();
		//File fi = new File("C:\\Users\\0100644068\\Documents\\Zalo Received Files\\01A800309607.tif");
		//File file = fi;
		try {
			try (InputStream is = ips) {
				try (ImageInputStream imageInputStream = ImageIO.createImageInputStream(is)) {
					Iterator<ImageReader> iterator = ImageIO.getImageReaders(imageInputStream);
					if (iterator == null || !iterator.hasNext()) {
						throw new RuntimeException(
								"Image file format not supported by ImageIO: ");
					}
					System.out.println("Reader File  Image: [ Done! ]");

					// We are just looking for the first reader compatible:
					ImageReader reader = iterator.next();
					reader.setInput(imageInputStream);
					int numPage = reader.getNumImages(true);
					System.out.println("numPage: [" + numPage + "]");

					IntStream.range(0, numPage).forEach(v -> {
						try {
							BufferedImage barCodeBufferedImage = reader.read(v);
							String indexPage = "" + v;
							// readBarcode(barCodeBufferedImage, "" + v);
							

							// ===========================================================================
							// Step 1: lấy tỉ lệ
							// Step 2: cắt theo tỉ lệ
							// Step 3: sau khi có hình cắt => đem đi đọc barcode
							// Step 4: Done!
							// ===========================================================================

							// tạo tỉ lệ tách hình thành bao nhiêu dòng và cột....
							
							int chunks = rows * cols;
							int chunkWidth = barCodeBufferedImage.getWidth() / cols; // Lấy kích thước tỉ lệ
							int chunkHeight = barCodeBufferedImage.getHeight() / rows;
							int count = 0;
							BufferedImage imgs[] = new BufferedImage[chunks];

							for (int x = 0; x < rows; x++) { // bắt đầu tách hình theo tỉ lệ
								for (int y = 0; y < cols; y++) {
									imgs[count] = new BufferedImage(chunkWidth, chunkHeight,
											barCodeBufferedImage.getType());
									// vẽ ra Graphics2D => sau khi vẽ xong sẽ trả ngược lại tỉ lệ vào imgs[i]
									Graphics2D gr = imgs[count++].createGraphics();
									gr.drawImage(barCodeBufferedImage, 0, 0, chunkWidth, chunkHeight, chunkWidth * y,
											chunkHeight * x, chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight,
											null);
									gr.dispose();
								}
							}

							// Sau khi chia tỉ lệ xong, ta lấy nó đi phân tích mã Barcode :)
							// Phần này nếu như đã xác định được mã bar code nằm ở góc nào rồi thì chỉ việc
							// lấy ra
							// ví dụ chia
							int checkListSize=0;
							try {
								checkListSize=pageIndexGetSplit.size();
							} catch (Exception e) {
								// TODO: handle exception
							}
							if(checkListSize>0) {
								for (int i = 0; i < checkListSize; i++) {
									try {
										String text = detectBarcodeToText(imgs[i]);
										if (!"".equals(text)) {
											Map map = new HashMap();
											map.put("pageIndex", indexPage);
											map.put("data", text);
											li.add(map);
											// System.out.println("page: ["+indexPage+" = "+readBarcode1(imgs[i])+"]");
										}
									} catch (ChecksumException e) {
										System.out.println("Lỗi ở vị trí thứ: [" + i + "]");
										System.out.println("Nguyên nhân lỗi: " + e.getCause());
									} catch (FormatException e) {
										System.out.println("Lỗi ở vị trí thứ: [" + i + "]");
										System.out.println("Nguyên nhân lỗi: " + e.getCause());
									}
								}
							}else {
								for (int i = 0; i < imgs.length; i++) {
									try {
										String text = detectBarcodeToText(imgs[i]);
										if (!"".equals(text)) {
											Map map = new HashMap();
											map.put("pageIndex", indexPage);
											map.put("data", text);
											li.add(map);
											// System.out.println("page: ["+indexPage+" = "+readBarcode1(imgs[i])+"]");
										}
									} catch (ChecksumException e) {
										System.out.println("Lỗi ở vị trí thứ: [" + i + "]");
										System.out.println("Nguyên nhân lỗi: " + e.getCause());
									} catch (FormatException e) {
										System.out.println("Lỗi ở vị trí thứ: [" + i + "]");
										System.out.println("Nguyên nhân lỗi: " + e.getCause());
									}
								}
							}
							
							// ===========================================================================
							
						} catch (IOException e) {
							e.printStackTrace();

						}
					});
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	
	return li;
	}

	public static List readBarcode(BufferedImage barCodeBufferedImage, String indexPage) {
		List<Map> li = new ArrayList();

		// ===========================================================================
		// Step 1: lấy tỉ lệ
		// Step 2: cắt theo tỉ lệ
		// Step 3: sau khi có hình cắt => đem đi đọc barcode
		// Step 4: Done!
		// ===========================================================================

		// tạo tỉ lệ tách hình thành bao nhiêu dòng và cột....
		int rows = 15;
		int cols = 2;
		int chunks = rows * cols;
		int chunkWidth = barCodeBufferedImage.getWidth() / cols; // Lấy kích thước tỉ lệ
		int chunkHeight = barCodeBufferedImage.getHeight() / rows;
		int count = 0;
		BufferedImage imgs[] = new BufferedImage[chunks];

		for (int x = 0; x < rows; x++) { // bắt đầu tách hình theo tỉ lệ
			for (int y = 0; y < cols; y++) {
				imgs[count] = new BufferedImage(chunkWidth, chunkHeight, barCodeBufferedImage.getType());
				// vẽ ra Graphics2D => sau khi vẽ xong sẽ trả ngược lại tỉ lệ vào imgs[i]
				Graphics2D gr = imgs[count++].createGraphics();
				gr.drawImage(barCodeBufferedImage, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x,
						chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight, null);
				gr.dispose();
			}
		}

		// Sau khi chia tỉ lệ xong, ta lấy nó đi phân tích mã Barcode :)
		// Phần này nếu như đã xác định được mã bar code nằm ở góc nào rồi thì chỉ việc
		// lấy ra
		// ví dụ chia
		for (int i = 0; i < imgs.length; i++) {
			try {
				ImageIO.write(imgs[i], "jpg", new File("D:/png/Spltting" + System.nanoTime() + i + ".jpg"));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				String text = detectBarcodeToText(imgs[i]);
				if (!"".equals(text)) {
					Map map = new HashMap();
					map.put("pageIndex", indexPage);
					map.put("data", text);
					li.add(map);
					// System.out.println("page: ["+indexPage+" = "+readBarcode1(imgs[i])+"]");
				}
			} catch (ChecksumException e) {
				System.out.println("Lỗi ở vị trí thứ: [" + i + "]");
				System.out.println("Nguyên nhân lỗi: " + e.getCause());
			} catch (FormatException e) {
				System.out.println("Lỗi ở vị trí thứ: [" + i + "]");
				System.out.println("Nguyên nhân lỗi: " + e.getCause());
			}
		}
		// ===========================================================================
		System.out.println(li);

		return li;

	}

	public static String detectBarcodeToText(BufferedImage image) throws ChecksumException, FormatException {
		String returnStr = "";
		LuminanceSource source = new BufferedImageLuminanceSource(image);
		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
		Reader reader = new MultiFormatReader();
		Result result = null;
		try {
			result = reader.decode(bitmap);
			returnStr = result.getText();
			// System.out.println("Barcode text is " + result.getText());
		} catch (NotFoundException e) {
			returnStr = "";
		}
		return returnStr;
	}
}
