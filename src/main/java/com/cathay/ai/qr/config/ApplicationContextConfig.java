package com.cathay.ai.qr.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan("com.cathay.ai.*") 
public class ApplicationContextConfig { 
 
    @Bean(name = "viewResolver")
    public InternalResourceViewResolver getViewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/public/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }
    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver commonsMultipartResolver(){
    	 final CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
         commonsMultipartResolver.setMaxUploadSize(-1);
         return commonsMultipartResolver;
    }
 

}