package com.cathay.ai.qr.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletRegistration;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import com.cathay.ai.aa.a0.helper.fileHelper;

public class SpringWebAppInitializer implements WebApplicationInitializer {
	private static Logger log = Logger.getLogger(SpringWebAppInitializer.class);
	private static String osName = ObjectUtils.toString(System.getProperties().get("os.name"));
	static String logImage = "                 \\||/\r\n" + "                 |  @___oo\r\n"
			+ "       /\\  /\\   / (__,,,,|\r\n" + "      ) /^\\) ^\\/ _)\r\n" + "      )   /^\\/   _)\r\n"
			+ "      )   _ /  / _)\r\n" + "  /\\  )/\\/ ||  | )_)\r\n" + " <  >      |(,,) )__)\r\n"
			+ "  ||      /    \\)___)\\\r\n" + "  | \\____(      )___) )___\r\n" + "   \\______(_______;;; __;;;\r\n"
			+ " =============================================JavaQrCode!!!============================================";
	public static void loadLog4j(String fileName) {
		BasicConfigurator.configure();
		File file;
		Properties props = new Properties();
		try {
			
			//fileOutputStream = new FileOutputStream("/opt/tomcat/webapps/JavaQrCode/WEB-INF/log4j.properties");
			
			if (osName.contains("Windows")) {
				file = new File(new File(".").getCanonicalPath() + "/" + fileName);
				if(!file.exists()) {
					fileHelper.createLog4jProperties();
					file = new File(new File(".").getCanonicalPath() + "/" + fileName);
				}
			} else {
				file = new File("/opt/tomcat/webapps/JavaQrCode/WEB-INF/" + fileName);
				if(!file.exists()) {
					fileHelper.createLog4jProperties();
					file = new File("/opt/tomcat/webapps/JavaQrCode/WEB-INF/" + fileName);
				}
			}
			
			props.load(new FileInputStream(file));
			PropertyConfigurator.configure(props);
			log.debug("Properties File Location: ["+file.getAbsolutePath()+"]");
		} catch (IOException e) {
			log.error("IOException: "+e.getCause());
			log.error(e);
		}
		
	//	return props;
	}
	static {
		log.fatal("=========================================================");
		BasicConfigurator.configure();
		loadLog4j("log4j.properties");
		log.fatal("BasicConfigurator: Coplete config log4j!!!");
		log.fatal("=========================================================");

	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
		appContext.register(ApplicationContextConfig.class);

		// Dispatcher Servlet
		ServletRegistration.Dynamic dispatcher = servletContext.addServlet("SpringDispatcher",
				new DispatcherServlet(appContext));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");

		dispatcher.setInitParameter("contextClass", appContext.getClass().getName());

		servletContext.addListener(new ContextLoaderListener(appContext));

		// UTF8 Charactor Filter.
		FilterRegistration.Dynamic fr = servletContext.addFilter("encodingFilter", CharacterEncodingFilter.class);

		fr.setInitParameter("encoding", "UTF-8");
		fr.setInitParameter("forceEncoding", "true");
		fr.addMappingForUrlPatterns(null, true, "/*");

		System.out.println(logImage);

	}


}
