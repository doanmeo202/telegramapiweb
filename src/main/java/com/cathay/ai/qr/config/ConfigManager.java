package com.cathay.ai.qr.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigManager {
	  private static Logger Log = Logger.getLogger(ConfigManager.class);
	  
	  private static final String ENCODE = "iso-8859-1";
	  
	  private static final Properties m_properties = new Properties();
	  
	  private static final String DEFAULT_PATH = "C:\\usr\\cxlcs\\config\\customconfig";
	  
	  private static final String DEFAULT_PATH_UNIX = "/usr/cxlcs/config/customconfig";
	  
	  public static String getConfigPath() {
	   // SystemContext sctx = SystemContext.getInstance();
	    String path = null;
	    try {
	     // path = sctx.getProperty("customConfigProperties");
	    	  Log.error(" path = sctx.getProperty(\"customConfigProperties\");");
	    } catch (Exception e) {
	      Log.error("cannot get property with key 'customConfigProperties', use default path ");
	    } 
	    if (path == null)
	      path = isWindow() ? "C:\\usr\\cxlcs\\config\\customconfig" : "/usr/cxlcs/config/customconfig"; 
	    return path;
	  }
	  
	  public static void loadProperties() {
	    String path = getConfigPath();
	    File initPath = new File(path);
	    m_properties.clear();
	    loadDir(initPath);
	  }
	  
	  public static String getProperty(String key) {
	    return getProperty(key, null);
	  }
	  
	  public static String getProperty(String key, String defaultValue) {
	    String encodStr = getProperty().getProperty(key, defaultValue);
	    try {
	      if (encodStr != null)
	        encodStr = new String(encodStr.getBytes("iso-8859-1")); 
	    } catch (UnsupportedEncodingException e) {
	      Log.fatal(e);
	    } 
	    return encodStr;
	  }
	  
	  public static Properties getProperty() {
	    if (m_properties.size() == 0)
	      initLoadProperties(); 
	    return m_properties;
	  }
	  
	  public static boolean isWindow() {
	    return System.getProperty("file.separator").equals("\\");
	  }
	  
	  private static void loadDir(File dir) {
	    File[] files = dir.listFiles();
	    if (files == null)
	      return; 
	    for (int i = 0; i < files.length; i++) {
	      if (files[i].isDirectory()) {
	        loadDir(files[i]);
	      } else if (files[i].isFile()) {
	        loadFile(files[i]);
	      } 
	    } 
	  }
	  
	  private static void loadFile(File file) {
	    if (file == null) {
	      Log.warn("loadFile file is null");
	      return;
	    } 
	    FileInputStream f = null;
	    if (file.getName().toLowerCase().endsWith(".properties"))
	      try {
	        f = new FileInputStream(file);
	        Properties p = new Properties();
	        p.load(f);
	        m_properties.putAll(p);
	      } catch (FileNotFoundException fnfe) {
	        Log.error(file.getAbsoluteFile() + " file not found");
	      } catch (IOException ioe) {
	        Log.error(ioe);
	      } finally {
	        if (f != null)
	          try {
	            f.close();
	          } catch (IOException e) {
	            Log.error(e);
	          }  
	      }  
	  }
	  
	  private static synchronized void initLoadProperties() {
	    if (m_properties.size() == 0)
	      loadProperties(); 
	  }}