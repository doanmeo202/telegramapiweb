<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>ApiQRcode</title>

<%@include file="../../../../public/layout/link.jsp"%>
<%
	String strContextPath = request.getContextPath();
%>

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Document</title>
<style type="text/css">
/* Flipping the video as it was not mirror view */
video {
	-webkit-transform: scaleX(-1);
	transform: scaleX(-1);
	margin-top: 5px;
}

/* Flipping the canvas image as it was not mirror view */
#canvas {
	-moz-transform: scaleX(-1);
	-o-transform: scaleX(-1);
	-webkit-transform: scaleX(-1);
	transform: scaleX(-1);
	filter: FlipH;
	-ms-filter: "FlipH";
}
</style>
</head>


<body>
	<a class="skippy sr-only sr-only-focusable" href="#content"> <span
		class="skippy-text">Trở về nội dung chính</span>
	</a>


	<%@include file="../../../../public/layout/header.jsp"%>
	<div id="reader"></div>

	<div class="container-fluid">
		<div class="row flex-xl-nowrap">

			<main class="col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content"
				role="main">
				
				<button id="btnScan" onclick="scanQrCode()" class="btn btn-outline-success">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
						fill="currentColor" class="bi bi-qr-code-scan" viewBox="0 0 16 16">
            <path
							d="M0 .5A.5.5 0 0 1 .5 0h3a.5.5 0 0 1 0 1H1v2.5a.5.5 0 0 1-1 0v-3Zm12 0a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0V1h-2.5a.5.5 0 0 1-.5-.5ZM.5 12a.5.5 0 0 1 .5.5V15h2.5a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5v-3a.5.5 0 0 1 .5-.5Zm15 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1 0-1H15v-2.5a.5.5 0 0 1 .5-.5ZM4 4h1v1H4V4Z" />
            <path d="M7 2H2v5h5V2ZM3 3h3v3H3V3Zm2 8H4v1h1v-1Z" />
            <path d="M7 9H2v5h5V9Zm-4 1h3v3H3v-3Zm8-6h1v1h-1V4Z" />
            <path
							d="M9 2h5v5H9V2Zm1 1v3h3V3h-3ZM8 8v2h1v1H8v1h2v-2h1v2h1v-1h2v-1h-3V8H8Zm2 2H9V9h1v1Zm4 2h-1v1h-2v1h3v-2Zm-4 2v-1H8v1h2Z" />
            <path d="M12 9h2V8h-2v1Z" />
        </svg>
				</button>
				<button onclick="sendData()" class="btn btn-outline-primary">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
						fill="currentColor" class="bi bi-send" viewBox="0 0 16 16">
            <path
							d="M15.854.146a.5.5 0 0 1 .11.54l-5.819 14.547a.75.75 0 0 1-1.329.124l-3.178-4.995L.643 7.184a.75.75 0 0 1 .124-1.33L15.314.037a.5.5 0 0 1 .54.11ZM6.636 10.07l2.761 4.338L14.13 2.576 6.636 10.07Zm6.787-8.201L1.591 6.602l4.339 2.76 7.494-7.493Z" />
        </svg>
				</button>
				<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script> -->
				<script
					src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
				<div class="jumbotron" style="margin-top: 20px; padding: 20px;">
					<p>
						<span id="errorMsg"></span>
					</p>
					<p>
						<span id="successMsg"></span>
					</p>
					<div class="row">
						<div class="col-lg-6">
							<!-- Here we streaming video from webcam -->

							<video id="video" playsinline autoplay></video>
							<canvas style="border: solid 1px #ddd; background-color: white;"
								id="canvas" width="475" height="475"></canvas>
						</div>


					</div>
				</div>
				<script type="text/javascript">
        var video = document.querySelector("#video");
        const constraints = {
            audio: false,
            video: {
                //facingMode: { exact: "environment" },
                width: 475,
                height: 475
            }
        };
        function sendData() {

            var destinationCanvas = document.createElement("canvas");
            var destCtx = destinationCanvas.getContext('2d');
            destinationCanvas.height = 500;
            destinationCanvas.width = 500;
            destCtx.translate(video.videoWidth, 0);
            destCtx.scale(-1, 1);
            destCtx.drawImage(document.getElementById("canvas"), 0, 0);
            // Get base64 data to send to server for upload  
            var imagebase64data = destinationCanvas.toDataURL("image/png");
            imagebase64data = imagebase64data.replace('data:image/png;base64,', '');
            //console.log(imagebase64data);
            var data = {
                imageString: imagebase64data
            };

            $.ajax({
                type: 'POST',
                url: '<%=strContextPath%>/qrcodeBase64',
                data: data,
                //contentType: 'application/json; charset=utf-8',
                //dataType: 'text',
                success: function (out) {

                }
            });


        }

        function timeout() {
            var canvas = document.getElementById('canvas');
            var context = canvas.getContext('2d');
            context.drawImage(video, 0, 0);
            var check = false;
            setTimeout(function () {
                var destinationCanvas = document.createElement("canvas");
                var destCtx = destinationCanvas.getContext('2d');
                destinationCanvas.height = 500;
                destinationCanvas.width = 500;
                destCtx.translate(video.videoWidth, 0);
                destCtx.scale(-1, 1);
                destCtx.drawImage(document.getElementById("canvas"), 0, 0);
                // Get base64 data to send to server for upload  
                var imagebase64data = destinationCanvas.toDataURL("image/png");
                imagebase64data = imagebase64data.replace('data:image/png;base64,', '');
                //console.log(imagebase64data);
                var data = {
                    imageString: imagebase64data
                };

                $.ajax({
                    type: 'POST',
                    url: 'http://192.168.31.116:8080/JavaQrCode//qrcodeBase64',
                    data: data,
                    //contentType: 'application/json; charset=utf-8',
                    //dataType: 'text',
                    success: function (out) {

                        returnData = (JSON.parse(out));
                        varQrdetect = JSON.parse(returnData.data)[0];
                        console.log(JSON.parse(returnData.data)[0]);
                        if (Number(varQrdetect.returnCode) < 0) {
                            check = false;
                        } else {
                            check = true;
                            $('#successMsg').html(varQrdetect.data);
                        }
                        if (check) {

                        } else {
                            timeout();
                        }
                    }
                });


            }, 500);
        }

        function scanQrCode() {
            if (navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia(constraints)
                    .then(function (stream) {
                        video.srcObject = stream;
                        timeout(); 
                        }).catch(function (err0r) {
                        alert("Lỗi: "+err0r);
                    });
            }
        }
        // Basic settings for the video to get from Webcam  


        // This condition will ask permission to user for Webcam access  


        function stop(e) {
            var stream = video.srcObject;
            var tracks = stream.getTracks();

            for (var i = 0; i < tracks.length; i++) {
                var track = tracks[i];
                track.stop();
            }
            video.srcObject = null;
        }
    </script>

				<script type="text/javascript">
        // Below code to capture image from Video tag (Webcam streaming)  
        $("#btnCapture").click(function () {
            var canvas = document.getElementById('canvas');
            var context = canvas.getContext('2d');

            // Capture the image into canvas from Webcam streaming Video element  
            context.drawImage(video, 0, 0);
        });

        // Upload image to server - ajax call - with the help of base64 data as a parameter  
        $("#btnSave").click(function () {

            // Below new canvas to generate flip/mirron image from existing canvas  
            var destinationCanvas = document.createElement("canvas");
            var destCtx = destinationCanvas.getContext('2d');


            destinationCanvas.height = 500;
            destinationCanvas.width = 500;

            destCtx.translate(video.videoWidth, 0);
            destCtx.scale(-1, 1);
            destCtx.drawImage(document.getElementById("canvas"), 0, 0);

            // Get base64 data to send to server for upload  
            var imagebase64data = destinationCanvas.toDataURL("image/png");
            imagebase64data = imagebase64data.replace('data:image/png;base64,', '');
            $.ajax({
                type: 'POST',
                url: '<%=strContextPath%>/qrcodeBase64',
                data: '{ "imageString" : "' + imagebase64data + '" }',
                contentType: 'application/json; charset=utf-8',
                dataType: 'text',
                success: function (out) {
                    alert('Image uploaded successfully..');
                }
            });
        });
    </script>
			</main>
		</div>
	</div>
</body>

</html>