<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>ApiQRcode</title>

<%@include file="../../../../public/layout/link.jsp"%>
<%
	String strContextPath = request.getContextPath();
%>
</head>

<body>
	<a class="skippy sr-only sr-only-focusable" href="#content"> <span
		class="skippy-text">Trở về nội dung chính</span>
	</a>


	<%@include file="../../../../public/layout/header.jsp"%>
	<div id="reader"></div>
	
	<div class="container-fluid">
		<div class="row flex-xl-nowrap">
			
			<main class="col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content"
				role="main">
				result:
				<div id="result"></div>
				</main>
		</div>
	</div>
	<script src="<%=strContextPath%>/js/html5-qrcode.min.js"></script>
	<script>
        const scanner = new Html5QrcodeScanner('reader', {
            // Scanner will be initialized in DOM inside element with id of 'reader'
            qrbox: {
                width: 150,
                height: 150,
            }, // Sets dimensions of scanning box (set relative to reader element width)
            fps: 60, // Frames per second to attempt a scan
        });


        scanner.render(success, error);
        // Starts scanner

        function success(result) {

            document.getElementById('result').innerHTML ='<h2>Success!</h2>'+result;
            // Prints result as a link inside result element

            scanner.clear();
            // Clears scanning instance

            document.getElementById('reader').remove();
            // Removes reader element from DOM since no longer needed

        }

        function error(err) {
            console.error(err);
            // Prints any errors to the console
        }
    </script>


</body>

</html>