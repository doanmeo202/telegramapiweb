<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>ApiQRcode</title>

<%@include file="../../../../public/layout/link.jsp"%>
<%
	String strContextPath = request.getContextPath();
%>
</head>

<body onload="checkTypeSend()">
	<a class="skippy sr-only sr-only-focusable" href="#content"> <span
		class="skippy-text">Trở về nội dung chính</span>
	</a>


	<%@include file="../../../../public/layout/header.jsp"%>

	<div class="container-fluid">
		<div class="row flex-xl-nowrap">
			<%@include file="../../../../public/layout/nav.jsp"%>
			<main class="col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content"
				role="main">
				<h2 id="css-files">
					<span class="bd-content-title">API Kiểm tra Online<a
						class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
						href="#css-files" style="padding-left: 0.375em;"></a></span>
				</h2>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th scope="col">Đường dẫn</th>
							<th scope="col">Phương thức</th>
							<th scope="col">Định dạng gửi đi</th>
							<th scope="col">Thông tin gửi đi</th>
							<th scope="col">Hành động</th>

						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">
								<div>
									<input id="txtUrl" type="text" style="width: 100%"
										value="http://27.64.26.210:8080/JavaQrCode/qrcode"
										name="txtUrl" />
								</div>
							</th>
							<td class="text-warning"><select id="methodSelect">
									<option value="POST">POST</option>
									<option value="GET">GET</option>
							</select></td>
							<td class="text-success"><select id="selecttype"
								onchange="checkTypeSend()">
									<option value="0">File Hình Ảnh</option>
									<option value="1">Mã Base64</option>
							</select></td>
							<td class="text-success">
								<div id="typeifsend"></div>
							</td>
							<td><button
									class="btn btn-susscess d-none d-lg-inline-block" id="btnButton"
									>Gửi</button></td>
						</tr>
					</tbody>
				</table>
				<section>
					<p>Kết quả trả về</p>
					<code>
						<span id="resurt"></span>
					</code>
				</section>
			</main>
		</div>
	</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
	<script
		src="https://getbootstrap.com/docs/4.4/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/docsearch.js@2/dist/cdn/docsearch.min.js"></script>
	<script src="https://getbootstrap.com/docs/4.4/assets/js/docs.min.js"></script>

	<script>
		function checkTypeSend() {
			var type = document.getElementById("selecttype").value;
			if (type == "0") {
				var file = '<input type="file" id="getFile" name="file"  accept="image/png, image/gif, image/jpeg"/>';
				document.getElementById("typeifsend").innerHTML = file;

			}
			if (type == "1") {
				//<input type="text" name="base64"/>
				var file = 'Đang Phát Triển Vui Lòng Chờ';
				document.getElementById("typeifsend").innerHTML = file;
			}
		}
	
		$(document).ready(function () {
            $("#btnButton").click(function () {
                var Obj = {};
                var type = document.getElementById("selecttype").value;
                var files=null;
                if (type == "0") {
                    files = document.getElementById("getFile").files[0];
                    Obj.file = files;
                    console.log(files);
                }

                var txtUrl = document.getElementById("txtUrl").value;
                Obj.txtUrl = txtUrl;
                var methodSelect = document.getElementById("methodSelect").value;
                Obj.methodSelect = methodSelect;
                var formData = new FormData();
                formData.append("file", files);
                console.log(Obj);
                $.ajax({
                    url: txtUrl,
                    type: Obj.methodSelect,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    enctype: 'multipart/form-data',
                	success: function (result) {
                	 $("#resurt").html(result);
                		},
               		 error: function (data) {
               			$("#resurt").html(data);
                }
                });
            });
        });
		
	</script>

</body>

</html>